﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace calculator
{
      
    public partial class MainPage : ContentPage
    {

        //using doubles to get decimal values
        double temp1 = 0;
        double temp2 = 0;
        double result = 0;
        string op = "";
        bool er = false;

        public MainPage()
        {
            InitializeComponent();
        }
        void OneClicked(object sender, System.EventArgs digit)
        {
            if (op == "")
            {
            
                temp1 = (temp1 * 10) + 1;
                displaylabel.Text = temp1.ToString();
            }
            else
            {   

                temp2 = (temp2 * 10) + 1;
                displaylabel.Text = (temp1 + op + temp2).ToString();


            }
        }
        void TwoClicked(object sender, System.EventArgs digit)
        {
            if (op == "")
            {
                temp1 = (temp1 * 10) + 2;
                displaylabel.Text = temp1.ToString();
            }
            else
            {
                temp2 = (temp2 * 10) + 2;
                displaylabel.Text = (temp1 + op + temp2).ToString();

            }
        }
        void ThreeClicked(object sender, System.EventArgs digit)
        {
            if (op == "")
            {
                temp1 = (temp1 * 10) + 3;
                displaylabel.Text = temp1.ToString();
            }
            else
            {
                temp2 = (temp2 * 10) + 3;
                displaylabel.Text = (temp1 + op + temp2).ToString();

            }
        }
        void FourClicked(object sender, System.EventArgs digit)
        {
            if (op == "")
            {
                temp1 = (temp1 * 10) + 4;
                displaylabel.Text = temp1.ToString();
            }
            else
            {

                temp2 = (temp2 * 10) + 4;
                displaylabel.Text = (temp1 + op + temp2).ToString();

            }
        }
        void FiveClicked(object sender, System.EventArgs digit)
        {
            if (op == "")
            {
                temp1 = (temp1 * 10) + 5;
                displaylabel.Text = temp1.ToString();
            }
            else
            {
                temp2 = (temp2 * 10) + 5;
                displaylabel.Text = (temp1 + op + temp2).ToString();
            }

        }
        void SixClicked(object sender, System.EventArgs digit)
        {
            if (op == "")
            {
                temp1 = (temp1 * 10) + 6;
                displaylabel.Text = temp1.ToString();
            }
            else
            {
                temp2 = (temp2 * 10) + 6;
                displaylabel.Text = (temp1 + op + temp2).ToString();
            }


        }
        void SevenClicked(object sender, System.EventArgs digit)
        {
            if (op == "")
            {
                temp1 = (temp1 * 10) + 7;
                displaylabel.Text = temp1.ToString();
            }
            else
            {
                temp2 = (temp2 * 10) + 7;
                displaylabel.Text = (temp1 + op + temp2).ToString();

            }

        }
        void EightClicked(object sender, System.EventArgs digit)
        {
            if (op == "")
            {
                temp1 = (temp1 * 10) + 8;
                displaylabel.Text = temp1.ToString();
            }
            else
            {
                temp2 = (temp2 * 10) + 8;
                displaylabel.Text = (temp1 + op + temp2).ToString();

            }
        }
        void NineClicked(object sender, System.EventArgs digit)
        {
            if (op == "")
            {
                temp1 = (temp1 * 10) + 9;
                displaylabel.Text = temp1.ToString();
            }
            else
            {
                temp2 = (temp2 * 10) + 9;
                displaylabel.Text = (temp1 + op + temp2).ToString();
            }
        }
        void ZeroClicked(object sender, System.EventArgs digit)
        {
            if (op == "")
            {
                temp1 = (temp1 * 10) + 0;
                displaylabel.Text = temp1.ToString();
            }
            else
            {
                temp2 = (temp2 * 10) + 0;
                displaylabel.Text = (temp1 + op + temp2).ToString();


            }
        }
        //--------------------------------------------------------------
        // I just reset everything and displayed the defualt which is 0
        void OnClearClicked(object sender, System.EventArgs args)
        {
            temp1 = 0;
            temp2 = 0;
            result = 0;
            op = "";
            er = false;

            displaylabel.Text = result.ToString();
        }
        //---------------------------------------------------------------
        //will show the inputs followed by the correct operator then
        //it will be stored in a string temp3 and thatswhats displayed
        // ex. (11+) 
        void PlusClicked(object sender, System.EventArgs args)
        {
            op = "+";

            displaylabel.Text = (temp1 + op).ToString();


        }
        void MinusClicked(object sender, System.EventArgs args)
        {
            op = "-";

            displaylabel.Text = (temp1 + op).ToString();

        }
        void DivisionClicked(object sender, System.EventArgs args)
        {
            op = "/";

            displaylabel.Text = (temp1 + op).ToString();


        }
        void MultClicked(object sender, System.EventArgs args)
        {
            op = "*";

            displaylabel.Text = (temp1 + op).ToString();

        }


        //----------------------------------------------------------------------------
        //based on which operator is used  it will do the math and display the result.
        //dividing by zero will show an error
        //you can continue using the result as the first number or clear and start over.
        void EqualsClicked(object sender, System.EventArgs args)
        {



            if (op == "+")
            {
                displaylabel.Text = (temp1 + temp2).ToString();
                temp1 = temp1 + temp2;
                temp2 = 0;
            }
            if (op == "-")
            {
                displaylabel.Text = (temp1 - temp2).ToString();
                temp1 = temp1 - temp2;
                temp2 = 0;
            }
            if (op == "*")
            {
                displaylabel.Text = (temp1 * temp2).ToString();
                temp1 = temp1 * temp2;
                temp2 = 0;
            }
            if (op == "/")
            {

                if (temp2 != 0)
                {
                    displaylabel.Text = (temp1 / temp2).ToString();
                    temp1 = temp1 / temp2;
                    temp2 = 0;
                }
                else
                {
                    er = true;
                }
            }

            if (er == true)
            {
                displaylabel.Text = "error";

                temp1 = 0;
                temp2 = 0;
                result = 0;
                op = "";
                er = false;
            }


        }


    }
}

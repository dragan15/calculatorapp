﻿using System;

using Xamarin.Forms;

namespace calculator
{
  
        public static class OperatorLogic
        {
            public static double Calculate(double value1, double value2, string mathop)
                {
                    double result = 0;
                    switch (mathop)
                     {
                        case "+":
                            result = value1 + value2;
                            break;
                        case "-":
                            result = value1 - value2;
                            break;
                        case "÷":
                            result = value1 / value2;
                            break;
                        case "×":
                            result = value1 * value2;
                            break;
                       
                    }
                  return result;
                }

       }
}


